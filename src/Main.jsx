import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Preview from "./Preview";
import Speed from "./Speed";
import KeyView from "./KeyView";
import "./App.css";
import SweetAlert from "react-bootstrap-sweetalert";

// const SweetAlert = require('react-bootstrap-sweetalert');

const initState = {
  text: "កខគឃង",
  //  text:
  //    "​កាំកុំកុះកេះកោះ ប្រទេសកម្ពុជាមានផ្ទៃដី ធំទូលំទូលាយ លាតសន្ធឹងនៅឆ្នេរអាស៊ីអាគ្នេយ៍",
  // text: "កាំខុំគុះឃេះងោះព្រះរាជាណាចក្រកម្ពុជា ដោយនាមសាមញ្ញ កម្ពុជា (កាំ-ពុ-ជា) ឬ ប្រទេសខ្មែរ ជារាជាធិបតេយ្យមួយស្ថិតនៅផ្នែកខាងត្បូងនៃឧបទ្វីបឥណ្ឌូចិន ក្នុងអនុតំបន់អាស៊ីអាគ្នេយ៍ ។ កម្ពុជាមានផ្ទៃក្រឡាសរុប ១៨១,០៣៥ សហាតិមាត្រការ៉េ ។",
  userInput: "",
  oldInput: "",
  symbols: 0,
  sec: 0,
  char: [],
  getTyping: [],
  color: "lightgray",
  isCorrect: true,
  started: false,
  finished: false,
  specChar1: "",
  specChar2: "",
  isSpecChar: true,
  accuracy: 0,
  time: 100,
  mainTime: 100,
  isResult: false,
  isAlert: false,
};

let count = 0;
let index = 0;
let inCorrect = 0;

export default class Main extends Component {
  constructor() {
    super();
    this.myRef = React.createRef();
    this.state = initState;
  }

  onRestart() {
    //this.setState(initState);
    window.location.reload();
  }

  onChange(e) {
    this.setTimer();
    this.onFinish(e.target.value);
    this.setState({
      userInput: e.target.value,
      symbols: this.countCorrectSymbols(e.target.value),
    });

    //***************************
    let getTyping = e.target.value;

    this.setState({
      getTyping: [],
    });
    getTyping.split("").forEach((i, index) => {
      this.state.getTyping.push(i.charCodeAt(0));
    });
    console.log("GETYPING: " + getTyping.length);
    index = getTyping.length;
    //console.log(this.state.getTyping);
    this.state.getTyping.map((item, index) => {
      console.log(
        item +
          "=" +
          this.state.char[index] +
          "=>" +
          String.fromCharCode(item) +
          "=" +
          String.fromCharCode(this.state.char[index])
      );

      if (item === this.state.char[index]) {
        console.log("True: " + index);
        this.setState({
          color: "rgb(31,188,12)",
          isCorrect: true,
        });
      } else {
        this.setState({
          color: "#ff4d4d",
          isCorrect: false,
        });
        count = 1;
        console.log("False: " + count);
      }
      if (getTyping === "") {
        this.setState({
          color: "lightgray",
        });
      }
    });

    if (count === 1) {
      inCorrect++;
      console.log("inCorrect: " + inCorrect);
      this.setState({
        isCorrect: false,
        color: "#ff4d4d",
      });
      count = 0;
      index = this.state.oldInput.length;
    }

    this.setState({
      accuracy: (index * 100) / (index + inCorrect),
    });

    this.setState({
      isSpecChar: true,
    });
    let specChar1 = this.state.char[index];
    //console.log("specChar1: "+specChar1)
    let specChar2 = this.state.char[index + 1];
    console.log("specChar2: " + specChar2);
    if (
      specChar1 == "6070" ||
      specChar1 == "6075" ||
      specChar1 == "6081" ||
      specChar1 == "6084"
    ) {
      if (specChar2 == "6086" || specChar2 == "6087") {
        this.setState({
          specChar1: specChar1,
          specChar2: specChar2,
          isSpecChar: false,
        });
      }
    }

    console.log("New: "+getTyping.length);
    console.log("Old: "+this.state.oldInput.length);
    if(getTyping.length < this.state.oldInput.length){
      this.setState({
        isCorrect: false,
      })
    }
  }

  onKeyDown(e) {
    console.log(
      "KeyDown: " + String.fromCharCode(e.keyCode) + ":" + e.target.value
    );
    this.setState({
      oldInput: e.target.value,
    });
    if (
      e.keyCode === 8 ||
      e.keyCode === 46 ||
      e.keyCode === 37 ||
      e.keyCode === 38 ||
      e.keyCode === 39 ||
      e.keyCode === 40 ||
      e.keyCode === 9
    ) {
      e.preventDefault();
      window.event.preventDefault();
    }

   
  }

  onKeyUp(e) {
    this.setState({
      color: "lightgray",
    });
    if (this.state.isCorrect === false) {
      this.setState({
        color: "crimson",
      });
    }
  }

  setTimer() {
    if (!this.state.started) {
      this.setState({ started: true });
      this.interval = setInterval(() => {
        this.setState((prev) => {
          return { sec: prev.sec + 1 };
        });
      }, 1000);
    }
  }

  onFinish(userInput) {
    console.log("Userinput: " + userInput);
    console.log("Text: " + this.state.text);
    if (userInput === this.state.text) {
      clearInterval(this.interval);
      clearInterval(this.myInterval);
      this.setState({
        mainTime: this.state.mainTime - this.state.time,
        finished: true,
        isAlert: true,
      });
      // window.scrollBy(0, 200);
    }
  }

  countCorrectSymbols(userInput) {
    const text = this.state.text;
    return userInput.split("").filter((s, i) => s === text[i]).length;
  }

  componentWillMount() {
    console.log("Length: " + this.state.text.length);
    //console.log(this.state.text.substr(0, this.state.text.length-1));
    this.state.text.split("").forEach((char, index) => {
      //console.log(char);
      this.state.char.push(char.charCodeAt(0));
    });
    //clearInterval(this.myInterval);
  }

  componentDidMount() {
    this.focusInput.focus();
    this.myInterval = setInterval(() => {
      this.setState((prev) => ({
        time: prev.time - 1,
      }));
      if (this.state.time == 0) {
        this.setState({
          //mainTime: this.state.mainTime - this.state.time,
          finished: true,
          isAlert: true,
        });
        clearInterval(this.myInterval);
        clearInterval(this.interval);
        //window.scrollBy(0, 200);
      }
    }, 1000);
  }

  handleScrollToElement() {
    window.scrollTo(0, this.myRef.current.offsetTop);
    // this.myRef.current.scrollIntoView({ behavior: "smooth" });
  }

  onConfirm() {
    this.setState({
      color: "lightgray",
      isAlert: false,
      isResult: true,
    });
    //window.scrollBy(0,500);
    this.handleScrollToElement();
  }

  onPrevent(e) {
    e.preventDefault();
    window.event.preventDefault();
  }

  onFocus(input) {
    this.focusInput = input;
  }

  render() {
    let alertResult = (
      <SweetAlert success title="បញ្ចប់!" onConfirm={this.onConfirm.bind(this)}>
        សូមចុច OK ដើម្បីពិនិត្យលទ្ធផល!
      </SweetAlert>
    );
    let myTime = `${String(Math.floor(this.state.time / 60)).padStart(
      2,
      "0"
    )}:${String(this.state.time % 60).padStart(2, "0")}`;
    let mainTime = `${String(Math.floor(this.state.mainTime / 60)).padStart(
      2,
      "0"
    )}:${String(this.state.mainTime % 60).padStart(2, "0")}`;

    let myAccuracy = `${this.state.accuracy.toFixed(2)}`;
    let wpm = <Speed sec={this.state.sec} symbols={this.state.symbols} />;
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-10">
            <h4 style={{ textAlign: "right" }}>
              <Speed sec={this.state.sec} symbols={this.state.symbols} />
            </h4>
            <h3>អត្ថបទ</h3>
            <Preview text={this.state.text} userInput={this.state.userInput} />

            <h3>សូមវាយបញ្ចូលទីនេះ</h3>
            <Form>
              <Form.Group controlId="formBasicEmail">
                {/*<Form.Label>Email address</Form.Label>*/}

                <textarea
                  id="myTextArea"
                  style={{
                    width: "100%",
                    rows: "2",
                    padding: "5px 15px",
                    borderRadius: "15px",
                    border: "1px solid lightgray",
                  }}
                  onContextMenu={this.onPrevent.bind(this)}
                  onDragStart={this.onPrevent.bind(this)}
                  //onMouseDown={this.onPrevent.bind(this)}
                  onSelect={this.onPrevent.bind(this)}
                  unselectable="on"
                  onPaste={this.onPrevent.bind(this)}
                  onCopy={this.onPrevent.bind(this)}
                  onCut={this.onPrevent.bind(this)}
                  autoComplete="off"
                  onDrag={this.onPrevent.bind(this)}
                  onDrop={this.onPrevent.bind(this)}
                  placeholder="ទីនេះ..."
                  ref={this.onFocus.bind(this)}
                  value={
                    // this.state.isCorrect
                    //   ? this.state.userInput
                    //   : this.state.userInput.substr(
                    //       0,
                    //       this.state.userInput.length - this.state.count
                    //     )

                    this.state.isCorrect
                      ? this.state.userInput
                      : this.state.oldInput
                  }
                  readOnly={this.state.finished}
                  onChange={this.onChange.bind(this)}
                  onKeyDown={this.onKeyDown.bind(this)}
                  onKeyUp={this.onKeyUp.bind(this)}
                  // style={{pointerEvents: 'none'}}
                ></textarea>
                <Form.Text className="text-muted">
                  {/*We'll never share your email with anyone else.*/}
                </Form.Text>
                <h3>គំរូក្តារចុច</h3>
                <h5 style={{ marginTop: "0" }}>
                  <KeyView
                    viewKey={this.state.char[index]}
                    nextKey={this.state.char[index + 1]}
                  />
                </h5>
              </Form.Group>

              {/* <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>
              <Form.Group controlId="formBasicCheckbox">
                <Form.Check type="checkbox" label="Check me out" />
                </Form.Group> */}
            </Form>
          </div>
          <div className="col-md-2">
            <h4>{myTime}</h4>
            <div
              style={{
                //borderColor: `${this.state.color}`,
                //border: `6px solid ${this.state.color}`,
                boxShadow: `0 4px 8px 0 ${this.state.color}, 0 6px 20px 0 ${this.state.color}`,
                borderRadius: "25px",
                height: "175px",
                fontSize: "50px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                transition: "border-color 75ms ease",
                margin: "25px 0",
              }}
            >
              {this.state.isSpecChar
                ? String.fromCharCode(this.state.char[index])
                : `${
                    String.fromCharCode(this.state.specChar1) +
                    "+" +
                    String.fromCharCode(this.state.specChar2)
                  }`}
            </div>
            <span>ត្រូវ</span>
            <h4>{index}</h4>
            <span>​​ខុស</span>
            <h4>{inCorrect}</h4>
            <span>សុក្រឹតភាព</span>
            <h4>{this.state.accuracy.toFixed(2)} %</h4>
          </div>
        </div>

        {this.state.isAlert ? alertResult : ""}
        <div
          //ref={this.myRef}
          className="row"
          style={{
            border: "1px solid lightgray",
            borderRadius: "15px",
            textAlign: "left",
            padding: "15px",
            visibility: `${this.state.isResult ? "visible" : "hidden"}`,
          }}
        >
          <div className="col-md-12">
            <div className="row">
              <div className="col-md-12">
                <h4>Dara</h4>
              </div>
            </div>
            <div className="row">
              <div className="col-md-5">
                <h5>• រយៈពេល​ ៖ {mainTime}</h5>
                <h5>• ល្បឿន​ (ពាក្យ/នាទី) ៖ {wpm}</h5>
              </div>
              <div className="col-md-3">
                <h5>• សរុប ៖ {index + inCorrect}</h5>
                <h5>• សុក្រឹតភាព ៖ {myAccuracy} %</h5>
              </div>
              <div className="col-md-2">
                <h5>• ត្រូវ ៖ {index}</h5>
                <h5>• ខុស ៖ {inCorrect}</h5>
              </div>
              <div
                className="col-md-2"
                style={{
                  display: "flex",
                  alignContent: "center",
                  justifyContent: "center",
                  marginBottom: "25px",
                }}
              >
                <Button
                  style={{ width: "200px" }}
                  variant="primary"
                  type="button"
                  onClick={this.onRestart.bind(this)}
                >
                  Restart
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <p ref={this.myRef}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Voluptatibus minima rerum expedita tempore labore quia amet, sequi
            quidem et accusamus qui culpa nesciunt eius nobis inventore sed
            doloremque asperiores esse.
          </p>
        </div>
      </div>
    );
  }
}
