import React, { Component } from "react";
import "./App.css";
import Main from "./Main";
import "bootstrap/dist/css/bootstrap.min.css";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Sign_in from './signin_form/index';
import Sign_up from './signup_form/index';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Main} /> 
            <Route exact path="/login" component={Sign_in} />
            <Route exact path="/signup" component={Sign_up} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
