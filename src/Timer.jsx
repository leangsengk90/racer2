import React, { Component } from "react";


//Not Use
export default class Timer extends Component {
  constructor() {
    super();
    this.state = {
      count: 5,
    };
  }
  
  componentDidMount() {
    this.myInterval = setInterval(() => {
      this.setState((prev) => ({
        count: prev.count - 1,
      }));
      if (this.state.count == 0) {
        clearInterval(this.myInterval);
      }
    }, 1000);
  }

  componentWillMount() {
    clearInterval(this.myInterval);
  }

  render() {
    return (
      <div>{`${String(Math.floor(this.state.count / 60)).padStart(
        2,
        "0"
      )}:${String(this.state.count % 60).padStart(2, "0")}`}</div>
    );
  }
}
