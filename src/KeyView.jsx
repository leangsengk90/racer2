import React from "react";

export default (props) => {
  // console.log("ViewKey: ",props.viewKey)
  let viewKey = "";
  switch (props.viewKey) {
    case 32:
      //ដកឃ្លា
      viewKey = "Shift នឹង Space (ដកឃ្លា)";
      break;
    case 8203:
      //ចន្លោះមើលមិនឃើញ
      viewKey = "Space (ចន្លោះមើលមិនឃើញ)";
      break;
    case 6098:
      //ដាក់ជើង
      viewKey = "j (ដាក់ជើង)";
      break;
    case 6016:
      //ក
      viewKey = "k";
      break;
    case 6017:
      //ខ
      viewKey = "x";
      break;
    case 6018:
       //គ
      viewKey = "Shift នឹង k";
      break;
    case 6019:
       //ឃ
      viewKey = "Shift នឹង x";
      break;
    case 6020:
       //ង
      viewKey = "g";
      break;
    case 6086:
       //  ំ
      viewKey = "Shift នឹង m";
      break;

    default:
      break;
  }
  //បើជា ស្រះ ា ឬ ស្រះ  ុ ឬ ស្រះ  េ ឬ ស្រះ  ោ
  if (
    props.viewKey == "6070" ||
    props.viewKey == "6075" ||
    props.viewKey == "6081" ||
    props.viewKey == "6084"
  ) {
     //បើជា ស្រះ  ំ នឹង ស្រះ  ះ
    if (props.nextKey == "6086" || props.nextKey == "6087") {
      if (props.viewKey == "6070" && props.nextKey == "6086") {
         //ស្រះ ាំ
        viewKey = "Shift នឹង a";
      } else if (props.viewKey == "6075" && props.nextKey == "6086") {
         //ស្រះ  ុំ
        viewKey = ",";
      } else if (props.viewKey == "6075" && props.nextKey == "6087") {
         //ស្រះ ​ ុះ
        viewKey = "Shift នឹង ,";
      } else if (props.viewKey == "6081" && props.nextKey == "6087") {
         //ស្រះ  េះ
        viewKey = "Shift នឹង v";
      } else if (props.viewKey == "6084" && props.nextKey == "6087") {
         //ស្រះ  ោះ
        viewKey = "Shift នឹង ;";
      }
    }
  }

  return (
    <div
      style={{
        border: "1px solid lightgray",
        borderRadius: "15px",
        width: "50%",
        height: "60px",
        padding: "15px 0",
        margin: "10px auto",
      }}
    >
      {viewKey}
    </div>
  );
};
